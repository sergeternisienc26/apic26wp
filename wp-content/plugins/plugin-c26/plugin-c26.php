<?php

/**
 * Plugin Name: Apprenants C26
 * Description: Apprenants Campus26 by Simplon
 * Author: Kyou
 */


function c26_app_init()
{
 
    $supports = array(
        'title',
        'excerpt',
        'thumbnail',
    );
 
    $args = array(
        'label'               => 'apprenants',
        'menu_icon'           => 'dashicons-welcome-learn-more',
        'show_in_rest'        => true,
        'public'              => true,
        'supports'            => $supports,
        'taxonomies' => array(
          'Promotions',
          'Competences'
        ),
 
    );
 
 
    $labelTaxoPromo = array(
        'name' => 'Promotions',
        'singular_name' => 'Promotion',
    );
 
    $argsTaxoPromo = array(
        'labels' => $labelTaxoPromo,
        'description' => 'Taxonomie des Promotions',
        'public' => true,
        'show_in_rest' => true,
        'hierarchical' => true,
    );
 
    $labelTaxoCompet = array(
        'name' => 'Competences',
        'singular_name' => 'Competence',
    );
 
    $argsTaxoCompet = array(
        'labels' => $labelTaxoCompet,
        'description' => 'Taxonomie des Compétences',
        'public' => true,
        'show_in_rest' => true,
        'hierarchical' => true,
    );
 

    
    register_taxonomy('promotions','apprenants', $argsTaxoPromo);
    register_taxonomy('competences','apprenants', $argsTaxoCompet);
    register_post_type('apprenants', $args);
};
 
add_action('init', 'c26_app_init');

// add this to functions.php
//register acf fields to Wordpress API
//https://support.advancedcustomfields.com/forums/topic/json-rest-api-and-acf/







/*==============
    METABOXE
  ==============  */

/*information wp ajout metaboxe*/
add_action('add_meta_boxes', 'init_metabox_appc26');
/*function appelée lors initialisation de l'admin */
function init_metabox_appc26()
{
    /*ajout de la metaboxe (id metaboxe,titre metaboxe,function metaboxe,slug contenu affiché*/
    add_meta_box('metabox_appc26', 'Informations', 'metabox_appc26', 'apprenants');
}
/*construction de la function metaboxe*/
function metabox_appc26($post)
{
    /*récup valeur actuelle pour mettre dans le champ*/
    $prenom = get_post_meta($post->ID, '_mon_prenom', true);
    $lien_linkendin = get_post_meta($post->ID, '_mon_lien_linkendin', true);
    $portfolio = get_post_meta($post->ID, '_mon_portfolio', true);
    $lien_cv = get_post_meta($post->ID, '_mon_lien_cv', true);
    /*affichage metaboxe*/
    echo '<label for="prenom">Prénom : </label>';
    echo '<input id="prenom" type="text" name="prenom" value="' . $prenom . '" /> ';
    echo '<label for="lien_linkendin">Lien Linkendin : </label>';
    echo '<input id="lien_linkendin" type="url" name="lien_linkendin" value="' . $lien_linkendin . '" />';
    echo '<label for="portfolio">Lien Portfolio : </label>';
    echo '<input id="portfolio" type="url" name="portfolio" value="' . $portfolio . '" />';
    echo '<label for="lien_cv">CV  : </label>';
    echo '<input id="lien_cv" type="url" name="lien_cv" value="' . $lien_cv . '" />';
}

/*sauvegarde données metabox */
add_action('save_post', 'save_metaboxes');
function save_metaboxes($post_id)
{
    /** si metaboxe définie(isset) alors sauvegarde de sa valeur */
    if (isset($_POST['prenom'])) {
        update_post_meta($post_id, '_mon_prenom', esc_html($_POST['prenom']));
    }
    if (isset($_POST['lien_linkendin'])) {
        update_post_meta($post_id, '_mon_lien_linkendin', esc_html($_POST['lien_linkendin']));
    }
    if (isset($_POST['portfolio'])) {
        update_post_meta($post_id, '_mon_portfolio', esc_html($_POST['portfolio']));
    }
    if (isset($_POST['lien_cv'])) {
        update_post_meta($post_id, '_mon_lien_cv', esc_html($_POST['lien_cv']));
    }
}

function filter_apprenantslist_json($data, $post, $context)
{
    $prenom = get_post_meta($post->ID, '_mon_prenom', true);
    $lien_linkendin = get_post_meta($post->ID, '_mon_lien_linkendin', true);
    $portfolio = get_post_meta($post->ID, '_mon_portfolio', true);
    $lien_cv = get_post_meta($post->ID, '_mon_lien_cv', true);

    if ($prenom) {
        $data->data['prenom'] = $prenom;
    }
    if ($lien_linkendin) {
        $data->data['lien_linkendin'] = $lien_linkendin;
    }

    if ($portfolio) {
        $data->data['portfolio'] = $portfolio;
    }

    if ($lien_cv) {
        $data->data['lien_cv'] = $lien_cv;
    }

    return $data;
}
add_filter('rest_prepare_apprenants', 'filter_apprenantslist_json', 10, 3);

/*function acf_to_rest_api($response, $post, $request) {
    if (!function_exists('get_fields')) return $response;

    if (isset($post)) {
        $acf = get_fields($post->id);
        $response->data['acf'] = $acf;
    }
    return $response;
}
add_filter('rest_prepare_apprenants', 'acf_to_rest_api', 10, 3);*/