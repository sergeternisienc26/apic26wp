<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en « wp-config.php » et remplir les
 * valeurs.
 *
 * Ce fichier contient les réglages de configuration suivants :
 *
 * Réglages MySQL
 * Préfixe de table
 * Clés secrètes
 * Langue utilisée
 * ABSPATH
 *
 * @link https://fr.wordpress.org/support/article/editing-wp-config-php/.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'apic26wp' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'root' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', '' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/**
 * Type de collation de la base de données.
 * N’y touchez que si vous savez ce que vous faites.
 */
define( 'DB_COLLATE', '' );

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clés secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '^YbqbpyU`5^hd~q5/C;us&kl?E6><=q!yt!gt5k}WRSThBVv<%Cu}x7Wrc/?[kS5' );
define( 'SECURE_AUTH_KEY',  'sl]wPJ]YJK]xwok@v:&(qL^ZIyc!_s//C ZM[>Z.UaSDH>|)C(Iu{3*N=+8TeO?l' );
define( 'LOGGED_IN_KEY',    'A_A6Vw{Hkl@TPH-nGd)!O>2.[eH7$~2M|BE`g0k@:N:B.*QkAJn*- QC.T*Rsx$4' );
define( 'NONCE_KEY',        '0N+WdVZ,XL(rtQ&fAtXL~5l0|<Pw`ZS9SXjuBl33rvx}OuhwO5,u$LG/|AlKJ*<>' );
define( 'AUTH_SALT',        'C}zh]))JGF_@h=-J4DYY&ld3Yo&-[,x2$#W(|(<OX.Dsu^#{*C+kA9nsd.oXH4[c' );
define( 'SECURE_AUTH_SALT', '6X_Prjb#t??tP]&TA6Pm!4N#OA_~ADQ%NX*R=|<){36D7ldUn)GAu-<f~UbnP{6:' );
define( 'LOGGED_IN_SALT',   ';G&J<CQ`G3+C1hFjpIy37Zp<H6X v(6w:6JkqWEF?k)(Xh<1 _Xg<+Cg=g2A-p(q' );
define( 'NONCE_SALT',       '90lnB&[6[)~zE*r-}_f8N0,uojz*5Z 6=l8l(5{!qk+aau:H>r<%vX9Y9jv_IZXY' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'ap_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortement recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://fr.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( ! defined( 'ABSPATH' ) )
  define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once( ABSPATH . 'wp-settings.php' );
